require 'bundler'
Bundler.require

require "logger"
require "yaml"

RACK_ENV ||= ENV["RACK_ENV"] || "development"
LOGGER = Logger.new(File.open("log/#{RACK_ENV}.log", "a+"))

require "./app/controllers/rewards_controller"
require "./app/services/eligibility_service_client"
require "./app/models/reward"
