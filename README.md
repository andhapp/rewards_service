# Rewards Service

This is a simple rewards service API

## Environment

1. Sinatra
2. Ruby 2.1.2 via RVM on Mac OSX
3. RSpec and rack-test for testing

## Running specs

1. Clone the repository using the following command:

```
git clone git@bitbucket.org:andhapp/rewards_service.git
```

2. Change into the cloned directory and install the gems using:

```
bundle install --path=vendor
``` 

Please make sure you have `bundle` available on the command line.

3. Run specs using:

```
bundle exec rspec spec
```
