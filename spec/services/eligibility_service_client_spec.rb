require 'spec_helper'

describe EligibilityServiceClient do

  context '.is_eligible?' do

    context 'if customer is eligible' do

      it 'should return true' do
        allow(EligibilityServiceClient).to receive(:status).and_return("CUSTOMER_ELIGIBLE")

        expect(EligibilityServiceClient.is_eligible?('valid-account-number')).to be_truthy
      end

    end

    context 'if customer is not eligible' do

      it 'should return false' do
        allow(EligibilityServiceClient).to receive(:status).and_return("CUSTOMER_INELIGIBLE")

        expect(EligibilityServiceClient.is_eligible?('valid-account-number')).to be_falsey
      end

    end

  end

end
