require 'spec_helper'

APP = Rack::Builder.parse_file('config.ru').first

describe 'Customer rewards' do

  def app
    APP
  end
  
  context 'invalid account number' do

    it 'should not return rewards' do
      allow(EligibilityServiceClient).to receive(:is_eligible?).with('invalid-account-number').
        and_raise(EligibilityServiceClient::InvalidAccountNumber.new("The supplied account number is invalid"))

      get "/rewards/invalid-account-number?channels[]=SPORTS&channels[]=NEWS"

      expect(last_response.status).to eql(200)
      expect(JSON.parse(last_response.body)).to eql({"rewards"=>[], "notice"=>"The supplied account number is invalid"})
    end

  end

  context 'technical failure' do

    it 'should not return rewards' do
      allow(EligibilityServiceClient).to receive(:is_eligible?).with('valid-account-number').
        and_raise(EligibilityServiceClient::ServiceUnavailable.new("Service unavailable"))

      get "/rewards/valid-account-number?channels[]=SPORTS&channels[]=NEWS"

      expect(last_response.status).to eql(200)
      expect(JSON.parse(last_response.body)).to eql({"rewards"=>[]})
    end

  end

  context 'customer is not legible' do

    it 'should not return rewards' do
      allow(EligibilityServiceClient).to receive(:is_eligible?).with('valid-account-number').
        and_return(false)

      get "/rewards/valid-account-number?channels[]=SPORTS&channels[]=NEWS"

      expect(last_response.status).to eql(200)
      expect(JSON.parse(last_response.body)).to eql({"rewards"=>[]})
    end

  end

  context 'customer is legible' do

    it 'should not return rewards' do
      allow(EligibilityServiceClient).to receive(:is_eligible?).with('valid-account-number').
        and_return(true)

      get "/rewards/valid-account-number?channels[]=SPORTS&channels[]=NEWS&channels[]=MOVIES"

      expect(last_response.status).to eql(200)
      expect(JSON.parse(last_response.body)).to eql({"rewards"=>['CHAMPIONS_LEAGUE_FINAL_TICKET', 'N/A', 'PIRATES_OF_THE_CARIBBEAN_COLLECTION']})
    end

  end
  
end
