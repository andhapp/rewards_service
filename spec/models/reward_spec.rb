require 'spec_helper'

describe Reward do

  describe '.get' do

    context 'for an invalid account number' do

      it 'should return no rewards and a reason for it' do
        allow(EligibilityServiceClient).to receive(:is_eligible?).
          with('invalid-account-number').
          and_raise(EligibilityServiceClient::InvalidAccountNumber.new('Invalid account'))

        expect(Reward.get('invalid-account-number', ['SPORTS'])).to eq(rewards: [], notice: 'Invalid account') 
      end
      
    end

    context 'when eligibility service is unavailable' do

      it 'should return no rewards' do
        allow(EligibilityServiceClient).to receive(:is_eligible?).
          with('valid-account-number').
          and_raise(EligibilityServiceClient::ServiceUnavailable.new('Service unavailable'))

        expect(Reward.get('valid-account-number', ['SPORTS'])).to eq(rewards: []) 
      end

    end

    context 'when customer is ineligible' do

      it 'should return no rewards' do
        allow(EligibilityServiceClient).to receive(:is_eligible?).
          with('valid-account-number').
          and_return(false)

        expect(Reward.get('valid-account-number', ['SPORTS'])).to eq(rewards: []) 
      end

    end
  
    context 'when customer is eligible' do

      it 'should return rewards' do
        allow(EligibilityServiceClient).to receive(:is_eligible?).
          with('valid-account-number').
          and_return(true)

        expect(Reward.get('valid-account-number', ['SPORTS'])).to eq(rewards: ['CHAMPIONS_LEAGUE_FINAL_TICKET']) 
      end

    end
    
  end

end
