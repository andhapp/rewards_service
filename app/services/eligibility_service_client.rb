class EligibilityServiceClient

  InvalidAccountNumber = Class.new(StandardError)
  ServiceUnavailable = Class.new(StandardError)
  CUSTOMER_ELIGIBLE = 'CUSTOMER_ELIGIBLE'

  def self.is_eligible?(account_number)
    status(account_number) == CUSTOMER_ELIGIBLE
  end

  private

  def self.status(account_number)
  end
  
end
