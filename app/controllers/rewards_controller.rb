require 'json'

class RewardsController < Sinatra::Base
  set :logging, true
  set :raise_errors, false
  set :show_exceptions, false

  before do
    content_type :json
  end

  get "/rewards/:account_number" do
    Reward.
      get(params[:account_number], params[:channels]).
      to_json
  end

end
