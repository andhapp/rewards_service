class Reward

  CHANNEL_REWARDS_MAPPING = {
    SPORTS: 'CHAMPIONS_LEAGUE_FINAL_TICKET',
    KIDS: 'N/A',
    MUSIC: 'KARAOKE_PRO_MICROPHONE',
    NEWS: 'N/A',
    MOVIES: 'PIRATES_OF_THE_CARIBBEAN_COLLECTION'
  }

  def self.get(account_number, channels)
    response = {rewards: []}

    begin
      if EligibilityServiceClient.is_eligible?(account_number)
        response[:rewards] = retrieve_rewards(channels)
      end

      response
    rescue EligibilityServiceClient::InvalidAccountNumber => e
      response.merge({notice: e.message})
    rescue EligibilityServiceClient::ServiceUnavailable => e
      response
    end
  end

  private
 
  def self.retrieve_rewards(channels)
    channels.map do |channel|
      CHANNEL_REWARDS_MAPPING[channel.to_sym]
    end    
  end
end
